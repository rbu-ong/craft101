<?php
namespace Craft;

class ContactMessagesVariable
{
    public function getAllMessages(){
        return craft()->contactMessages_main->getAllMessages();
    }
}