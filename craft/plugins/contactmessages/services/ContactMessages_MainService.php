<?php
namespace Craft;

class ContactMessages_MainService extends BaseApplicationComponent
{
    public function saveContactMessages( ContactMessages_MainModel $contact )
    {

    	$contactMessages = new ContactMessages_MainRecord();

    	$contactMessages->firstname = $contact->firstname;
		$contactMessages->lastname = $contact->lastname;
		$contactMessages->emailaddress = $contact->emailaddress;
		$contactMessages->companyname = $contact->companyname;
		$contactMessages->phonenumber = $contact->phonenumber;
		$contactMessages->messagebody = $contact->messagebody;
		$contactMessages->isQuestion = $contact->isQuestion;

    	$contactMessages->save( false );
    }

    public function getAllMessages( $id = null ){
    	return ( $id ) ? $this->_createSectionQuery( $id )->queryRow() : $this->_createSectionQuery( $id )->queryAll();
    }

    private function _createSectionQuery( $id )
    {
		return craft()->db->createCommand()
			->select('*')
			->where(( $id ) ? array( 'id' => $id ) : '1=1')
			->from('contactmessages');
	}
}