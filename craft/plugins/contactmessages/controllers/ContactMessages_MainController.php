<?php
namespace Craft;

class ContactMessages_MainController extends BaseController
{
    public function actionSaveMessage()
    {

    	$contact = new ContactMessages_MainModel();

		$contact->firstname = craft()->request->getPost('firstname');
		$contact->lastname = craft()->request->getPost('lastname');
		$contact->emailaddress = craft()->request->getPost('emailaddress');
		$contact->companyname = craft()->request->getPost('companyname');
		$contact->phonenumber = craft()->request->getPost('phonenumber');
		$contact->messagebody = craft()->request->getPost('messagebody');
		$contact->isQuestion = craft()->request->getPost('isQuestion');

    	craft()->contactMessages_main->saveContactMessages($contact);
    }

    public function actionResponse( array $variables = [] ){
    	$variables['messagedetails'] = craft()->contactMessages_main->getAllMessages($variables['messageID']);

    	$this->renderTemplate('contactmessages/_response', $variables );
    }
    
}