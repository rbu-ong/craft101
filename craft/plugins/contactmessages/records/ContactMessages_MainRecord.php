<?php
namespace Craft;

class ContactMessages_MainRecord extends BaseRecord
{
    public function getTableName()
    {
        return 'contactmessages';
    }

    protected function defineAttributes()
    {
        return array(
            'firstname' => array(AttributeType::String, 'maxLength' => 50)
            ,'lastname' => array(AttributeType::String, 'maxLength' => 50)
            ,'emailaddress' => array(AttributeType::String, 'maxLength' => 50)
            ,'companyname' => array(AttributeType::String, 'maxLength' => 50)
            ,'phonenumber' => array(AttributeType::String, 'maxLength' => 50)
            ,'messagebody' => array(AttributeType::String, 'column' => ColumnType::Text,  'required' => false)
            ,'isQuestion' => array(AttributeType::Number, 'default' => 0, 'maxLength' => 1)
        );
    }
}