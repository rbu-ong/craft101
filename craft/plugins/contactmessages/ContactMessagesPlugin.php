<?php
namespace Craft;

class ContactMessagesPlugin extends BasePlugin
{

    function getName()
    {
         return Craft::t('Contact Messages');
    }

    function getVersion()
    {
        return '1.0';
    }

    function getDeveloper()
    {
        return 'M 2 M';
    }

    function getDeveloperUrl()
    {
        return 'http://google.com';
    }

    public function hasCpSection()
    {
        return true;
    }

    public function getSettingsHtml()
    {
        return craft()->templates->render('contactmessages');
    }

    public function prepSettings($settings)
    {
        // Modify $settings here...

        return $settings;
    }

    public function registerCpRoutes()
    {
        return array(
            'contactmessages/(?P<messageID>\d+)' => array('action' => 'contactMessages/Main/response'),
        );
    }

}