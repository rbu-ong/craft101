$(document).ready(function(){

	$('.navbar li').click(function(e) {
		var $this = $(this);

		$('.navbar li.active').removeClass('active');
		 
		if (!$this.hasClass('active')) {
			$this.addClass('active');
		}
	});

	$('.navbar li a').click('a[href^="#"]', function (event) {

		var base = $.attr(this, 'href').split('')
		var temp = window.location.href.split(window.location.origin)

		var current = temp[1].split('')

		if( ( base[0] == '#' && current[0] == '#' ) || current.length == 1 || ( window.location.href + '/' ) == window.location.origin ){
			$('html, body').animate({
		        scrollTop: $($.attr(this, 'href')).offset().top
		    }, 1000);

		    event.preventDefault();
		}
		else{
			window.location = window.location.origin + $.attr(this, 'href')		
		}

	});

	$("#contactmessage-form").submit( function(e) {

    	e.preventDefault();

	    var data = $(this).serialize();

	    $.ajax({
	        method: 'POST',
	        url: '/',
	        data: data,
	        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	        success: function( result ){

	        }
	    });

	});

	// init Isotope
	var $grid = $('.grid').isotope({
	  itemSelector: '.element-item',
	  layoutMode: 'fitRows',
	  getSortData: {
	    category: '[data-category]'
	  }
	});

	// filter functions
	var filterFns = {
	  // show if number is greater than 50
	  numberGreaterThan50: function() {
	    var number = $(this).find('.number').text();
	    return parseInt( number, 10 ) > 50;
	  },
	  // show if name ends with -ium
	  ium: function() {
	    var name = $(this).find('.name').text();
	    return name.match( /ium$/ );
	  }
	};

	// bind filter button click
	$('#filters').on( 'click', 'li', function() {
	  var filterValue = ( $( this ).attr('data-filter') == '*' ) ? $( this ).attr('data-filter') : '.' + $( this ).attr('data-filter');
	  // use filterFn if matches value
	  filterValue = filterFns[ filterValue ] || filterValue;
	  $grid.isotope({ filter: filterValue });
	});

});